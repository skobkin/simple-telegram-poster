<?php

namespace App;

use App\Controller\PostController;
use Symfony\Component\HttpFoundation\{Request, Response};
use Symfony\Component\HttpFoundation\Exception\RequestExceptionInterface;

final class AppKernel
{
    public const KERNEL_ROOT = __DIR__;
    public const PROJECT_ROOT = __DIR__.'/..';

    public static function handle(Request $request): void
    {
        $response = new Response();

        try {
            $response = static::handleRequest($request);
        } catch (RequestExceptionInterface $e) {
            $response->setStatusCode($e->getCode());
            $response->setContent($e->getMessage());
        } catch (\Throwable $e) {
            $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
            $response->setContent('Server error: '.$e->getMessage().'<br />'.$e->getTraceAsString());
        }

        $response->send();
    }

    private static function handleRequest(Request $request): Response
    {
        return PostController::post($request);
    }
}