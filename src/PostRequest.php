<?php

namespace App;

use \Symfony\Component\Validator\Constraints as Assert;

class PostRequest
{
    /**
     * @var string
     *
     * @Assert\Choice({"Markdown", "HTML", ""})
     */
    public $format;

    /**
     * @var int
     *
     * @Assert\NotBlank(message="Can not be blank")
     */
    public $channel;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="Can not be blank")
     * @Assert\Length(max="4000", maxMessage="Maximum length exceeded.")
     */
    public $content;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="Password can not be blank")
     * @Assert\Length(min="8", minMessage="Password is shorter than allowed.")
     */
    public $password;
}