<?php

namespace App\Service;


use App\AppKernel;
use App\Telegram\MessageSender;
use Symfony\Component\Templating\{Loader\FilesystemLoader, PhpEngine, TemplateNameParser};
use Symfony\Component\Validator\{Validation, Validator\ValidatorInterface};
use Symfony\Component\Yaml\Yaml;
use unreal4u\TelegramAPI\TgLog;

class PseudoContainer
{
    private const TEMPLATES_PATH = AppKernel::PROJECT_ROOT.'/templates';
    private const CONFIG_PATH = AppKernel::PROJECT_ROOT.'/config/parameters.yml';

    /** @var MessageSender[] */
    private static $messageSendersByToken = [];

    /** @var ValidatorInterface */
    private static $validator = null;

    /** @var PhpEngine */
    private static $templater = null;

    /** @var array|null */
    private static $config = null;

    public static function getMessageSender(string $token): MessageSender
    {
        if (!array_key_exists($token, static::$messageSendersByToken)) {
            $tgClient = new TgLog($token);
            return static::$messageSendersByToken[$token] = new MessageSender($tgClient);
        }

        return static::$messageSendersByToken[$token];
    }

    public static function getValidator(): ValidatorInterface
    {
        if (!static::$validator) {
            return static::$validator = Validation::createValidatorBuilder()
                ->enableAnnotationMapping()
                ->getValidator()
            ;
        }

        return static::$validator;
    }

    public static function getTemplater(): PhpEngine
    {
        if (!static::$templater) {
            $loader = new FilesystemLoader(static::TEMPLATES_PATH.'/%name%');

            return static::$templater = new PhpEngine(new TemplateNameParser(), $loader);
        }

        return static::$templater;
    }

    public static function getTelegramToken(): string
    {
        $config = static::getConfig();

        if ($config && array_key_exists('telegram_token', $config)) {
            return $config['telegram_token'];
        }

        throw static::createConfigReadException('Telegram token');
    }

    public static function getTelegramChats(): array
    {
        $config = static::getConfig();

        if ($config && array_key_exists('telegram_chats', $config) && is_array($config['telegram_chats'])) {
            return $config['telegram_chats'];
        }

        throw static::createConfigReadException('Telegram chats');
    }

    public static function getPassword(): string
    {
        $config = static::getConfig();

        if ($config && array_key_exists('password', $config)) {
            return $config['password'];
        }

        throw static::createConfigReadException('Password');
    }

    public static function getConfig(): array
    {
        if (!static::$config) {
            return static::$config = Yaml::parseFile(static::CONFIG_PATH);
        }

        return static::$config;
    }

    private static function createConfigReadException(string $subject)
    {
        return new \RuntimeException(sprintf('%s not found in the config or file could not be read.', $subject));
    }
}