<?php

namespace App\Exception;


use Symfony\Component\HttpFoundation\Exception\RequestExceptionInterface;

class AccessDeniedException extends \Exception implements RequestExceptionInterface
{

}