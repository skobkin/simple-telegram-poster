<?php

namespace App\Telegram;

use GuzzleHttp\Exception\ClientException;
use unreal4u\TelegramAPI\Abstracts\KeyboardMethods;
use unreal4u\TelegramAPI\Telegram\Methods\SendMessage;
use unreal4u\TelegramAPI\TgLog;

/**
 * Service which sends simple messages to Telegram users
 */
class MessageSender
{
    public const PARSE_MODES = [
        'Markdown' => 'Markdown',
        'HTML' => 'HTML',
        'Plaintext' => '',
    ];

    /** @var TgLog */
    private $client;

    /**
     * @param TgLog $client
     */
    public function __construct(TgLog $client)
    {
        $this->client = $client;
    }

    public function sendMessageToChat(
        int $chatId,
        string $text,
        string $parseMode = self::PARSE_MODES['Plaintext'],
        KeyboardMethods $keyboardMarkup = null,
        bool $disableWebPreview = false,
        bool $disableNotifications = false
    ): bool {
        if (!in_array($parseMode, self::PARSE_MODES)) {
            throw new \InvalidArgumentException('Invalid $parseMode.');
        }

        $sendMessage = new SendMessage();
        $sendMessage->chat_id = (string)$chatId;
        $sendMessage->text = $text;
        $sendMessage->parse_mode = $parseMode;
        $sendMessage->disable_web_page_preview = $disableWebPreview;
        $sendMessage->disable_notification = $disableNotifications;
        $sendMessage->reply_markup = $keyboardMarkup;

        try {
            $this->client->performApiRequest($sendMessage);

            return true;
        } catch (ClientException $e) {
            return false;
        }
    }
}