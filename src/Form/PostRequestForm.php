<?php

namespace App\Form;

use App\PostRequest;
use \Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\Form\Extension\Core\Type\{ChoiceType, PasswordType, TextareaType};
use \Symfony\Component\OptionsResolver\OptionsResolver;

class PostRequestForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('format', ChoiceType::class, [
                'label' => 'Format',
                'choices' => $options['parse_modes'],
            ])
            ->add('channel', ChoiceType::class, [
                'label' => 'Channel',
                'choices' => $options['channels'],
            ])
            ->add('content', TextareaType::class, ['label' => 'Text'])
            ->add('password', PasswordType::class, ['label' => 'Password'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        // Channels must be provided in ['name' => id] format
        $resolver->setRequired(['channels', 'parse_modes']);
        $resolver->setDefault('data_class', PostRequest::class);
    }

    public function getBlockPrefix()
    {
        return 'post';
    }
}