<?php

namespace App\Controller;

use App\Exception\AccessDeniedException;
use App\Form\PostRequestForm;
use App\PostRequest;
use App\Service\PseudoContainer;
use App\Telegram\MessageSender;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\HttpFoundation\HttpFoundationExtension;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Forms;
use Symfony\Component\HttpFoundation\{Request, Response};

final class PostController
{
    public static function post(Request $request): Response
    {
        $form = static::createPostForm(PseudoContainer::getTelegramChats(), MessageSender::PARSE_MODES);

        $form->handleRequest($request);

        $sent = false;

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var PostRequest $post */
            $post = $form->getData();

            if (PseudoContainer::getPassword() !== $post->password) {
                throw new AccessDeniedException('Invalid password', Response::HTTP_FORBIDDEN);
            }

            $messageSender = PseudoContainer::getMessageSender(PseudoContainer::getTelegramToken());

            $sent = $messageSender->sendMessageToChat(
                $post->channel,
                $post->content,
                $post->format
            );
        }

        $templater = PseudoContainer::getTemplater();

        return new Response($templater->render('post_form.php', [
            'form' => $form->createView(),
            'sent' => $sent,
        ]));
    }

    private static function createPostForm(array $channels, array $formats): FormInterface
    {
        $formFactory = Forms::createFormFactoryBuilder()
            ->addExtension(new HttpFoundationExtension())
            ->addExtension(new ValidatorExtension(PseudoContainer::getValidator()))
            ->getFormFactory()
        ;

        $form = $formFactory->create(PostRequestForm::class, new PostRequest(), [
            'channels' => $channels,
            'parse_modes' => $formats,
        ]);
        $form->add('submit', SubmitType::class, ['label' => 'Send']);

        return $form;
    }
}