<?php

use Doctrine\Common\Annotations\AnnotationRegistry;

/** @var \Composer\Autoload\ClassLoader $loader */
$loader = require __DIR__.'/../vendor/autoload.php';

// For annotations
AnnotationRegistry::registerLoader([$loader, 'loadClass']);

return $loader;