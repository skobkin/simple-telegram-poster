# Simple Telegram poster form

## Installation

```bash
# For usage (from Packagist)
composer create-project skobkin/simple-telegram-poster
# For development (from Git)
git clone git@bitbucket.org:skobkin/telegram-poster-form.git
cd telegram-poster-form && composer install
```

## Configuration

```bash
cp config/parameters.yml.dist config/parameters.yml
```

Edit `config/parameters.yml` configuration file according to your needs.

## Running

```bash
# From the project root directory
php -S localhost:8000 -t public
```
