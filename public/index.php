<?php

namespace App;


use \Symfony\Component\HttpFoundation\Request;

/** @var \Composer\Autoload\ClassLoader $loader */
$loader = require __DIR__.'/../src/autoload.php';

$request = Request::createFromGlobals();

AppKernel::handle($request);