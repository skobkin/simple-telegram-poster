<?php
/** @var \Symfony\Component\Form\FormView $form */
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Post to the Telegram</title>

    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://yastatic.net/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script >
</head>

<body>
<h1>Telegram Poster</h1>

<div id="post-form">
    <form
            id="<?php echo $form->vars['id']; ?>"
            name="<?php echo $form->vars['name']; ?>"
            action="<?php echo $form->vars['action']; ?>"
            method="<?php echo $form->vars['method']; ?>"
    >

        <table>
            <tr>
                <td>
                    <label for="<?php echo $form->offsetGet('format')->vars['id']; ?>">
                        <?php echo $form->offsetGet('format')->vars['label']; ?>
                    </label>
                </td>
                <td>
                    <!-- Post Format -->
                    <select
                            id="<?php echo $form->offsetGet('format')->vars['id']; ?>"
                            name="<?php echo $form->offsetGet('format')->vars['full_name']; ?>"
                            <?php if ($form->offsetGet('format')->vars['required']): ?>required<?php endif; ?>
                    >
                        <?php /** @var \Symfony\Component\Form\ChoiceList\View\ChoiceView $choice */ ?>
                        <?php foreach ($form->offsetGet('format')->vars['choices'] as $choice): ?>
                            <option
                                <?php if ($choice->value == $form->offsetGet('format')->vars['value']): ?>selected<?php endif; ?>
                                value="<?php echo $choice->value; ?>"
                            >
                                <?php echo $choice->label; ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </td>
                <td>
                    <label for="<?php echo $form->offsetGet('channel')->vars['id']; ?>">
                        <?php echo $form->offsetGet('channel')->vars['label']; ?>
                    </label>
                </td>
                <td>
                    <!-- Channel -->
                    <select
                            id="<?php echo $form->offsetGet('channel')->vars['id']; ?>"
                            name="<?php echo $form->offsetGet('channel')->vars['full_name']; ?>"
                            <?php if ($form->offsetGet('channel')->vars['required']): ?>required<?php endif; ?>
                    >
                        <?php /** @var \Symfony\Component\Form\ChoiceList\View\ChoiceView $choice */ ?>
                        <?php foreach ($form->offsetGet('channel')->vars['choices'] as $choice): ?>
                            <option
                                    <?php if ($choice->value === $form->offsetGet('channel')->vars['value']): ?>selected<?php endif; ?>
                                    value="<?php echo $choice->value; ?>"
                            >
                                <?php echo $choice->label; ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <?php
                    /** @var \Symfony\Component\Form\FormErrorIterator $contentErrors */
                    $contentErrors = $form->offsetGet('content')->vars['errors'];
                    foreach ($contentErrors as $error): ?>
                        <span><?php echo $error->getMessage(); ?></span>
                    <?php endforeach; ?>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="<?php echo $form->offsetGet('content')->vars['id']; ?>">
                        <?php echo $form->offsetGet('content')->vars['label']; ?>
                    </label>
                </td>
                <td colspan="3">
                    <!-- Post Content -->
                    <textarea
                            id="<?php echo $form->offsetGet('content')->vars['id']; ?>"
                            name="<?php echo $form->offsetGet('content')->vars['full_name']; ?>"
                            <?php if ($form->offsetGet('content')->vars['required']): ?>required<?php endif; ?>
                    ><?php echo $form->offsetGet('content')->vars['value']; ?></textarea>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <?php
                    /** @var \Symfony\Component\Form\FormErrorIterator $contentErrors */
                    $contentErrors = $form->offsetGet('password')->vars['errors'];
                    foreach ($contentErrors as $error): ?>
                        <span><?php echo $error->getMessage(); ?></span>
                    <?php endforeach; ?>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="<?php echo $form->offsetGet('password')->vars['id']; ?>">
                        <?php echo $form->offsetGet('password')->vars['label']; ?>
                    </label>
                </td>
                <td colspan="3">
                    <!-- Security password -->
                    <input
                            type="password"
                            id="<?php echo $form->offsetGet('password')->vars['id']; ?>"
                            name="<?php echo $form->offsetGet('password')->vars['full_name']; ?>"
                            <?php if ($form->offsetGet('password')->vars['required']): ?>required<?php endif; ?>
                    ><?php echo $form->offsetGet('password')->vars['value']; ?></input>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <input
                            type="submit"
                            id="<?php echo $form->offsetGet('submit')->vars['id']; ?>"
                            name="<?php echo $form->offsetGet('submit')->vars['full_name']; ?>"
                            <?php if ($form->offsetGet('channel')->vars['disabled']): ?>disabled<?php endif; ?>
                            value="<?php echo $form->offsetGet('submit')->vars['label']; ?>"
                    />
                </td>
            </tr>
        </table>
    </form>

    <div id="sent-status">
        <span>
            <?php if ($sent): ?>
            Sent
            <?php else: ?>
            Not sent
            <?php endif; ?>
        </span>
    </div>
</div>

<span><a href="https://skobk.in/">skobk.in</a></span>
</body>
</html>